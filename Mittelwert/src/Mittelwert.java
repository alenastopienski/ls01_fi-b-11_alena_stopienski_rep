import java.util.Scanner;

public class Mittelwert {

	public static void main(String[] args) {

		//Deklaration von Variablen
		double anzahl;
		double zahl;
		double summe=0;
		double mwert;
		
		//Eingabe
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Dieses Programm berechnet den Mittelwert der eingegebenen Zahlen.");
		
		System.out.print("Bitte geben Sie die Anzahl der Zahlen ein: ");
		anzahl = myScanner.nextDouble();
		
		for (int i=1; i<=anzahl; i++) {
		System.out.print("Bitte geben Sie die Zahl ein: ");
		zahl = myScanner.nextDouble();
		summe += zahl;
		}
		
		//Verarbeitung
		mwert = summe/anzahl;
		
		//Ausgabe
		System.out.println("Mittelwert: " + mwert);
		
		myScanner.close();
	}

}
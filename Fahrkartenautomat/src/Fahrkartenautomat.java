﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       int tickettyp;   
       double ticketpreis;
       double anzahltickets;
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       double rückgabebetrag;
              
       while (true) {
       zuZahlenderBetrag=fahrkartenbestellungErfassen();
       rückgabebetrag=fahrkartenBezahlen(zuZahlenderBetrag);
       fahrkartenAusgeben();
       rueckgeldAusgeben(rückgabebetrag);
       newsession();
       }
    }

           
    public static double fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
    	double zuZahlenderBetrag;
    	int tickettyp;
    	double ticketpreis=0;
    	byte anzahltickets;
    	System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus: \n" + "  Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\n"
    			+ "  Tageskarte Regeltarif AB [8,60 EUR] (2)\n"
    			+ "  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
    	do {
    	System.out.println("Ihre Wahl: ");
    	tickettyp=tastatur.nextInt();
    	
    	switch(tickettyp) {
    	case 1:
    		ticketpreis=2.90;
    		break;
    	case 2:
    		ticketpreis=8.60;
    		break;
    	case 3:
    		ticketpreis=23.50;
    		break;
    	default:
    		System.out.println(">>falsche Eingabe<<");
    		break;
    	}
    	}
    	while(tickettyp>3);
    	
        System.out.println("Anzahl der Tickets: ");
        anzahltickets = tastatur.nextByte();
        do {
        System.out.println(">>Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
        System.out.println("Anzahl der Tickets: ");
        anzahltickets = tastatur.nextByte();
        }
        while(anzahltickets>10);
               
        zuZahlenderBetrag = ticketpreis * anzahltickets;
        return zuZahlenderBetrag;
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	Scanner tastatur = new Scanner(System.in);
    	double eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   zuZahlenderBetrag=zuZahlenderBetrag - eingezahlterGesamtbetrag;
     	   System.out.printf("Noch zu zahlen: %.2f Euro \n", zuZahlenderBetrag);
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   double eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        return rückgabebetrag;
    }
    
    public static void fahrkartenAusgeben() {
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double rückgabebetrag) {
    	 if(rückgabebetrag > 0.0)
         {
      	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro \n", rückgabebetrag);
      	   System.out.println("wird in folgenden Münzen ausgezahlt:");

             while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
             {
          	  System.out.println("2 EURO");
  	          rückgabebetrag -= 2.0;
             }
             while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
             {
          	  System.out.println("1 EURO");
  	          rückgabebetrag -= 1.0;
             }
             while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
             {
          	  System.out.println("50 CENT");
  	          rückgabebetrag -= 0.5;
             }
             while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
             {
          	  System.out.println("20 CENT");
   	          rückgabebetrag -= 0.2;
             }
             while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
             {
          	  System.out.println("10 CENT");
  	          rückgabebetrag -= 0.1;
  	          
             }
             while(Math.round(rückgabebetrag*100.0)/100.0 >= 0.05)// 5 CENT-Münzen
             {
          	  System.out.println("5 CENT");
   	          rückgabebetrag -= 0.05;
             }
             System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                            "vor Fahrtantritt entwerten zu lassen!\n"+
                            "Wir wünschen Ihnen eine gute Fahrt.");
         }
    }
     public static void newsession() {
         	System.out.println("\nStarting new session...");
             for (int i = 0; i < 8; i++)
             {
                System.out.print("=");
                try {
      			Thread.sleep(250);
      		} catch (InterruptedException e) {
      			e.printStackTrace();
      		}
             }
             System.out.println("\n\n");
         }
    }

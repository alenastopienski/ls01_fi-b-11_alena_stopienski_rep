/**Class "Cargo"*/
public class Cargo {

	/**Attributes of cargo class*/
	/**Name of cargo*/
	private String cargoName;
	
	/**Type of cargo*/
	private String cargoType;
	
	/**Amount of cargo*/
	private int cargoAmount;
	
	/**Standard constructor of cargo*/
	public Cargo() {
		this.cargoName = null;
		this.cargoType = null;
		this.cargoAmount = 0;
	}
	
	/**Full-parameterized constructor of cargo
	 * @param cargoName Name of the cargo unit
	 * @param cargoType Type of the cargo unit
	 * @param cargoAmount Amount of cargo */
	public Cargo(String cargoName, String cargoType, int cargoAmount) {
		setCargoName(cargoName);
		setCargoType(cargoType);
		setCargoAmount(cargoAmount);
	}
	
	public void setCargoName(String cargoName) {
		this.cargoName = cargoName;
	}
	
	public String getCargoName() {
		return cargoName;
	}
	
	public void setCargoType(String cargoType) {
		this.cargoType = cargoType;
	}
	
	public String getCargoType() {
		return cargoType;
	}
	
	public void setCargoAmount(int cargoAmount) {
		this.cargoAmount = cargoAmount;
	}
	
	public int getCargoAmount() {
		return cargoAmount;
	}
	
	/**Method, that prints all attributes of cargo*/
	@Override
	public String toString() {
		return this.cargoName + " " + this.cargoType + " " + this.cargoAmount;
	}

}

import java.util.ArrayList;

/** Class "Spaceship" */
public class Spaceship {

	/**Attributes of a spaceship class*/
	/**Name of a spaceship*/
	private String shipName;
	
	/**Energy of a spaceship in %*/
	private int energyProcent;
	
	/**Shield of a spaceship in %*/
	private int shieldProcent;
	
	/**Life support of a spaceship in %*/
	private int lifeSupportProcent;
	
	/**Hull of a spaceship in %*/
	private int hullProcent;
	
	/**Photon torpedo amount of a spaceship*/
	private int photonTorpedoAmount;
	
	/**Repair android amount of a spaceship*/
	private int repairAndroidAmount;
	
	/**Cargo list of a spaceship*/
	private ArrayList<Cargo> cargoOnBoard = new ArrayList<Cargo>();
	
	/**Broadcast communicator, that is for every spaceship available. It saves all the messages sent from ships into space*/
	private static ArrayList<String> broadcastCommunicator = new ArrayList<String>();
	
	/**Constructors of a spaceship*/
	/**Standard constructor*/
	public Spaceship() {
		this.shipName = null;
		this.energyProcent = 0;
		this.shieldProcent = 0;
		this.lifeSupportProcent = 0;
		this.hullProcent = 0;
		this.photonTorpedoAmount = 0;
		this.repairAndroidAmount = 0;
	}
	
	/**Full-parameterized constructor
	 * @param shipName Name of a spaceship
	 * @param energyProcent Energy of a spaceship in %
	 * @param shieldProcent Shield of a spaceship in %
	 * @param lifeSupportProcent Life support of a spaceship in %
	 * @param hullProcent Hull of a spaceship in %
	 * @param photonTorpedoAmount Photon torpedo amount of a spaceship
	 * @param repairAndroidAmount Repair android amount of a spaceship
	 * */
	public Spaceship(String shipName, int energyProcent, int shieldProcent, int lifeSupportProcent, int hullProcent, int photonTorpedoAmount, int repairAndroidAmount) {
		setShipName(shipName);
		setEnergyProcent(energyProcent);
		setShieldProcent(shieldProcent);
		setLifeSupportProcent(lifeSupportProcent);
		setHullProcent(hullProcent);
		setPhotonTorpedoAmount(photonTorpedoAmount);
		setRepairAndroidAmount(repairAndroidAmount);		
	}
	
	/**
	 * @param shipName
	 */
	public void setShipName(String shipName) {
		this.shipName = shipName;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getShipName() {
		return shipName;
	}
	
	public void setEnergyProcent(int energyProcent) {
		this.energyProcent = energyProcent;
	}
	
	public int getEnergyProcent() {
		return energyProcent;
	}
	
	public void setShieldProcent(int shieldProcent) {
		this.shieldProcent = shieldProcent;
	}
	
	public int getShieldProcent() {
		return shieldProcent;
	}
	
	public void setLifeSupportProcent(int lifeSupportProcent) {
		this.lifeSupportProcent = lifeSupportProcent;
	}
	
	public int getLifeSupportProcent() {
		return lifeSupportProcent;
	}
	
	public void setHullProcent(int hullProcent) {
		this.hullProcent = hullProcent;
	}
	
	public int getHullProcent() {
		return hullProcent;
	}
	
	public void setPhotonTorpedoAmount(int photonTorpedoAmount) {
		this.photonTorpedoAmount = photonTorpedoAmount;
	}
	
	public int getPhotonTorpedoAmount() {
		return photonTorpedoAmount;
	}
	
	public void setRepairAndroidAmount(int repairAndroidAmount) {
		this.repairAndroidAmount = repairAndroidAmount;
	}
	
	public int getRepairAndroidAmount() {
		return repairAndroidAmount;
	}
	
	/**Method, that adds cargo to a cargo list
	 * @param newCargo Cargo unit to be added to a spaceship*/
	public void addCargo(Cargo newCargo) {
		cargoOnBoard.add(newCargo);
	}
	
	/**Method, that launches a photon torpedo.
	 * If there're no torpedoes on board, other ships get a message "Click".
	 * If there're some torpedoes on board, the amount of them is reduced by 1 and all other ships get a message, that torpedo was launched.
	 * @param targetShip Target spaceship to be damaged with a photon torpedo */
	public void photonTorpedoFire(Spaceship targetShip) {
		if (this.photonTorpedoAmount == 0) {
			messageToAll("-=*Click*=-");			
		} else {
			this.photonTorpedoAmount = this.photonTorpedoAmount - 1;
			messageToAll("Photon Torpedo launched...");
			damageReceived(targetShip);
		}
	}
	
	public void photonTorpedoLoad(int photonTorpedoAmount) {
		
	}
	
	/**Method, that fires a phaser cannon.
	 * If there's less than 50% of energy, other ships get a message "Click".
	 * If there's enough energy, the amount of energy is reduced by 50 and all other ships get a message, that a phaser cannon fired.
	 * @param targetShip Target spaceship to be damaged with a phaser cannon */
	public void phaserCannonFire(Spaceship targetShip) {
		if (this.energyProcent < 50) {
			messageToAll("-=*Click*=-");
		} else {
			this.energyProcent = this.energyProcent - 50;
			messageToAll("Phaser Cannon fired...");
			damageReceived(targetShip);
		}
	}
	
	/**Method, that checks if any damage is received by a target ship.
	 * It changes the attributes of a spaceship depending on damage received
	 * If shieldProcent equals 0, then hullProcent and energyProcent will be reduced by 50%.
	 * If hullProcent as a result of the previous If clause equals 0, then lifeSupportProcent gets a value 0 and the ship sends a message into space, that its life support system was destroyed.
	 * If shieldProcent is bigger than 0, then shieldProcent is reduced by 50%.
	 * @param targetShip Target spaceship to be damaged */
	private void damageReceived(Spaceship targetShip) {
		System.out.println(targetShip.getShipName() + " is receiving damage!");
		if (shieldProcent == 0) {
			hullProcent = hullProcent - 50;
			energyProcent = energyProcent - 50;
			if (hullProcent == 0) {
				lifeSupportProcent = 0;
				messageToAll(targetShip.getShipName() + ": Life support system is destroyed");
			}
		} else {
		shieldProcent = shieldProcent - 50;
		}
	}
	
	/**Method, that sends a message from a spaceship into space and saves this message in a broadcast communicator*/
	public void messageToAll(String message) {
		System.out.println(message);
		broadcastCommunicator.add(message);
	}
	
	/**Method, that prints all messages saved in broadcast communicator*/
	public static ArrayList<String> logShow() {
		System.out.println("\n Message log: ");
		for(String s : broadcastCommunicator) {
			System.out.println(s);
		}
		return broadcastCommunicator;
	}
	
	public void repairShip() {
		
	}
	
	/**Method, that prints all attributes of a spaceship*/
	@Override
	public String toString() {
		return "\n Name of the ship: " + this.shipName + "\n Energy: " + this.energyProcent + "\n Hull: " + this.hullProcent + "\n Shields: " + this.shieldProcent + "\n Life Support: " + this.lifeSupportProcent + "\n Photon Torpedos: " + this.photonTorpedoAmount + "\n Repair Androids: " + this.repairAndroidAmount;
	}
	
	/**Method, that prints all cargo on board*/
	public void cargoShow() {
		System.out.println("\n Cargo on board: ");
		for(Cargo cargo : cargoOnBoard) {
			System.out.printf("\n Cargo name: " + cargo.getCargoName() + "\n Cargo type: " + cargo.getCargoType() + "\n Cargo amount: " + cargo.getCargoAmount() + "\n");
		}
	}
	
	public void cargoJettison() {
		
	}
	
	
}
	


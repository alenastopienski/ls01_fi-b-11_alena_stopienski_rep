/**Testing spaceship and cargo interactions*/
public class TestSpaceship {

	public static void main(String[] args) {
		/**Creating 3 spaceships-objects*/
		Spaceship klingonen = new Spaceship("IKS Hegh'ta", 100, 100, 100, 100, 1, 2);
		Spaceship romulaner = new Spaceship("IRW Khazara", 100, 100, 100, 100, 2, 2);
		Spaceship vulkanier = new Spaceship("Ni'Var", 80, 80, 100, 50, 0, 5);
		
		/**Creating cargo-objects*/
		Cargo carKlingonen1 = new Cargo("Ferengi Schneckensaft", "food supplies", 200);
		Cargo carKlingonen2 = new Cargo("Bat'leth Klingonenschwert", "weapons", 200);
		Cargo carRomulaner1 = new Cargo("Borg-Schrott", "scrap", 5);
		Cargo carRomulaner2 = new Cargo("Rote Materie", "unknown", 2);
		Cargo carRomulaner3 = new Cargo("Plasma-Waffe", "weapons", 50);
		Cargo carVulkanier1 = new Cargo("Forschungssonde", "exploration equipment", 35);
		Cargo carVulkanier2 = new Cargo("Photonentorpedo", "weapons", 3);
		
		/**Assigning cargo-objects to spaceships-objects*/
		klingonen.addCargo(carKlingonen1);
		klingonen.addCargo(carKlingonen2);
		romulaner.addCargo(carRomulaner1);
		romulaner.addCargo(carRomulaner2);
		romulaner.addCargo(carRomulaner3);
		vulkanier.addCargo(carVulkanier1);
		vulkanier.addCargo(carVulkanier2);
	
		/**Clingon ship fires a torpedo at a romulan ship*/
		klingonen.photonTorpedoFire(romulaner);
		
		/**Romulan ship fires at clingon ship with a phaser cannon*/
		romulaner.phaserCannonFire(klingonen);
		
		/**Vulcan ship sends a message into space*/
		vulkanier.messageToAll(vulkanier.getShipName() + ": Violence is illogical");
		
		/**Clingon ship prints their status out*/
		System.out.println("\n" + klingonen.getShipName() + " status: \n" + klingonen + "\n");
		
		/**Clingon ship prints their cargo out*/
		klingonen.cargoShow();
		
		/**Clingon ship fires a torpedo at a romulan ship*/
		klingonen.photonTorpedoFire(romulaner);
		
		/**Clingon ship fires a torpedo at a romulan ship*/
		klingonen.photonTorpedoFire(romulaner);
		
		/**Clingon ship prints their status out*/
		System.out.println("\n" + klingonen.getShipName() + " status: \n" + klingonen + "\n");
		
		/**Clingon ship prints their cargo out*/
		klingonen.cargoShow();
		
		/**Romulan ship prints their status out*/
		System.out.println("\n" + romulaner.getShipName() + " status: \n" + romulaner + "\n");
		
		/**Romulan ship prints their cargo out*/
		romulaner.cargoShow();
		
		/**Vulcan ship prints their status out*/
		System.out.println("\n" + vulkanier.getShipName() + " status: \n" + vulkanier + "\n");
		
		/**Vulcan ship prints their cargo out*/
		vulkanier.cargoShow();
		
		/**Printing all messages saved in broadcast communicator*/
		Spaceship.logShow();
		
		

	}
}

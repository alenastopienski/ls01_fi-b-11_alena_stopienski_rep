import java.util.Scanner;

public class MittelwertmitMethoden {

	public static void main(String[] args) {
		
		double anzahl;
		double zahl;
		double summe=0;
		double m;
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Dieses Programm berechnet den Mittelwert der eingegebenen Zahlen.");
				
		anzahl=eingabe(myScanner, "Bitte geben Sie die Anzahl der Zahlen ein:");
		
		for (int i=1; i<=anzahl; i++) {
		zahl=eingabe(myScanner, "Bitte geben Sie die Zahl ein: ");
		summe += zahl;
		}
		
		m = berechneMittelwert(summe, anzahl);
		ausgabe(m);
		myScanner.close();
	}

	public static double eingabe(Scanner ms, String text) {
		System.out.println(text);
		double zahl = ms.nextDouble();
		return zahl;
	}
	
	public static double berechneMittelwert(double summe, double anzahl) {
		double m = summe/anzahl;
		return m;
	}
	
	public static void ausgabe(double mittelwert) {
		System.out.println("Mittelwert: " + mittelwert);
	}
}

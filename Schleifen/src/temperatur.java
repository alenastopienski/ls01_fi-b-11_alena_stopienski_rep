import java.util.Scanner;

public class temperatur {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Bitte den Startwert in Celsius eingeben: ");
		double startcel=sc.nextDouble();
		System.out.println("Bitte den Endwert in Celsius eingeben:");
		double endcel=sc.nextDouble();
		System.out.println("Bitte die Schrittweite in Grad Celsius eingeben:");
		double step=sc.nextDouble();
		double f = 0;
		
		while (startcel<endcel) {
			System.out.printf("Celsius: " + startcel + " ");
			startcel=startcel+step;
			f=(startcel*9/5)+32;
			System.out.printf("Fahrenheit: " + f + "\n");
		}

	}

}

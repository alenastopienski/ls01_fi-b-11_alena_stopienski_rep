import java.util.Arrays;
import java.util.Scanner;
public class MittelwertMitSchleife {

	public static void main(String[] args) {
		// (E) "Eingabe"
				// Werte X und Y festlegen:
				// ===========================
				Scanner tastatur = new Scanner(System.in);
				System.out.println("Von wie vielen Zahlen soll der Mittelwert berechnet werden?");
				int anzahl = tastatur.nextInt();
				double mittelwert = 0;
				double[] zahlen = new double[anzahl];
				

				// (V) Verarbeitung
				// Mittelwert von x und y berechnen:
				// ================================
				for (int i = 0; i < zahlen.length; i++) {
					System.out.println("Bitte gebe eine Zahl f�r die Mittelwertberechnung ein: ");
					zahlen[i] = tastatur.nextDouble();
					mittelwert = mittelwert + zahlen[i];
				}
				mittelwert /= zahlen.length;
				
				//Ausgabe:
				System.out.println("Der Mittelwert lautet: " + mittelwert);
				System.out.println("Eingegebene Zahlen: " + Arrays.toString(zahlen));
				

	}

}

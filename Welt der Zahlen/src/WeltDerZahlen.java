/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 25.10.2021
  * @author << Stopienski Alena >>
  */

public class WeltDerZahlen {

	public static void main(String[] args) {
		/*  *********************************************************
	    
        Zuerst werden die Variablen mit den Werten festgelegt!
   
   *********************************************************** */
   // Im Internet gefunden ?
   // Die Anzahl der Planeten in unserem Sonnesystem                    
   byte anzahlPlaneten = 9 ;
   
   // Anzahl der Sterne in unserer Milchstra�e
   long anzahlSterne = 400000000000l;
   
   // Wie viele Einwohner hat Berlin?
    int bewohnerBerlin = 3645000;
   
   // Wie alt bist du?  Wie viele Tage sind das?
   
     short alterTage = 11314;
   
   // Wie viel wiegt das schwerste Tier der Welt?
   // Schreiben Sie das Gewicht in Kilogramm auf!
     int gewichtKilogramm = 190000;
   
   // Schreiben Sie auf, wie viele km� das gr��te Land der Erde hat?
      int flaecheGroessteLand = 17098242;
   
   // Wie gro� ist das kleinste Land der Erde?
   
      float flaecheKleinsteLand = 0.44f;
   
   
   
   
   /*  *********************************************************
   
        Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
   
   *********************************************************** */
   
   System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
   
   System.out.println("Anzahl der Sterne: " + anzahlSterne);
   
   System.out.println("Anzahl der Bewohner in Berlin: " + bewohnerBerlin);
   
   System.out.println("Anzahl der Tage: " + alterTage);
   
   System.out.println("Gewicht des schwersten Tieres: " + gewichtKilogramm);
   
   System.out.println("Gr��e des gr��sten Land: " + flaecheGroessteLand);
   
   System.out.println("Gr��e des kleinsten Land: " + flaecheKleinsteLand);
   
   System.out.println(" *******  Ende des Programms  ******* ");

	}

}

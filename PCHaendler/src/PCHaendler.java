import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		
		String artikel;
		int anzahl;
		double preis;
		double mwst;
		double nettogesamtpreis;
		double bruttogesamtpreis;
		
		Scanner myScanner = new Scanner(System.in);
		
		artikel=liesString(myScanner, "Was moechten Sie bestellen?");
		anzahl=liesInt(myScanner, "Geben Sie die Anzahl ein: ");
		preis=liesDouble(myScanner, "Geben Sie den Nettopreis ein: ");
		mwst=liesDouble(myScanner, "Geben Sie den Mehrwertsteuersatz in Prozent ein: ");
		nettogesamtpreis=berechneGesamtnettopreis(anzahl, preis);
		bruttogesamtpreis=berechneGesamtbruttopreis(nettogesamtpreis, mwst);
		rechungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
		myScanner.close();
		
		//Original code:
		
		// Benutzereingaben lesen
		//System.out.println("was moechten Sie bestellen?");
		//String artikel = myScanner.next();

		//System.out.println("Geben Sie die Anzahl ein:");
		//int anzahl = myScanner.nextInt();

		//System.out.println("Geben Sie den Nettopreis ein:");
		//double preis = myScanner.nextDouble();

		//System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		//double mwst = myScanner.nextDouble();

		// Verarbeiten
		//double nettogesamtpreis = anzahl * preis;
		//double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);

		// Ausgeben

		//System.out.println("\tRechnung");
		//System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		//System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
		
		//End of original code
		
		}
	
	public static String liesString(Scanner ms, String text) {
		System.out.println(text);
		String artikel = ms.next();
		return artikel;
		
	}
	
	public static int liesInt(Scanner ms, String text) {
		System.out.println(text);
		int anzahl = ms.nextInt();
		return anzahl;
	}
	
	public static double liesDouble(Scanner ms, String text) {
		System.out.println(text);
		double preis=ms.nextDouble();
		return preis;
	}
	
	
	public static double berechneGesamtnettopreis(int anzahl, double preis) {
		double nettogesamtpreis = anzahl * preis;
		return nettogesamtpreis;
	}
	
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
		return bruttogesamtpreis;
	}
	
	public static void rechungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}


}
import java.util.Scanner;
public class Rechner {

	public static void main(String[] args) {
		// Neues Scanner-Objekt myScanner wird erstellt
		 Scanner myScanner = new Scanner(System.in);

		 System.out.print("Bitte geben Sie eine ganze Zahl ein: ");

		 // Die Variable zahl1 speichert die erste Eingabe
		 int zahl1 = myScanner.nextInt();

		 System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");

		 // Die Variable zahl2 speichert die zweite Eingabe
		 int zahl2 = myScanner.nextInt();

		 // Die Addition der Variablen zahl1 und zahl2
		 // wird der Variable ergebnis zugewiesen.
		 int addition = zahl1 + zahl2;

		 System.out.print("\n\n\nErgebnis der Addition lautet: ");
		 System.out.print(zahl1 + " + " + zahl2 + " = " + addition);
		 
		 //substraction
		 int substraktion = zahl1 - zahl2;
		 
		 System.out.print("\n\n\nErgebnis der Substraktion lautet: ");
		 System.out.print(zahl1 + " - " + zahl2 + " = " + substraktion);
		 
		 //multiplikation
		 int multiplikation = zahl1 * zahl2;
		 
		 System.out.print("\n\n\nErgebnis der Multiplikation lautet: ");
		 System.out.print(zahl1 + " * " + zahl2 + " = " + multiplikation);
		 
		 //division
		 double zahl1div=zahl1;
		 double division = zahl1div / zahl2;
		 
		 System.out.print("\n\n\nErgebnis der Division lautet: ");
		 System.out.print(zahl1 + " / " + zahl2 + " = " + division);
		 
		 myScanner.close();

		 }
		}

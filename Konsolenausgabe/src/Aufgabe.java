
public class Aufgabe {

	public static void main(String[] args) {
		//System.out.println("Hallo! Wie geht es dir?");
		//System.out.println("Yo! \b Test1");
		//System.out.println("Yo! \t Test1");
		//System.out.println("Yo! \n Test1");
		//System.out.println("Yo! \f Test1");
		//System.out.println("Yo! \r Test1");
		//System.out.println("Yo! \" Test1");
		//System.out.println("Yo! \' Test1");
		//System.out.println("Yo! \\ Test1");
		//String s="Java Program";
		//int n=123;
		//double k=12.345;
		//System.out.printf( "\n|%s|\n", s );
		//System.out.printf( "|%20s|\n", s );
		//System.out.printf( "|%-20s|\n", s );
		//System.out.printf( "|%20.4s|\n", s );
		//System.out.printf("\n|%+-15d| |%15d|", n, -n);
		//System.out.printf("\n%f", k);
		
		System.out.println("\nAufgabe 1\n");		
		System.out.println("Ich bin ein Gummi-B�r. Ich fresse nur Haribo."); //Gummi-B�r kann auch kommentieren
		System.out.println("Ich bin ein Gummi-B�r.\nIch fresse nur Haribo."); //print() schreibt alles in einem Linie, println() in einem Absatz
		System.out.println("Ich bin ein Gummi-B�r.\tIch fresse nur Haribo.");
		System.out.println("Ich bin ein Gummi-B�r.\rIch fresse nur \"Haribo\".");
		String s="Ich bin ein Gummi-B�r";
		System.out.printf("%50s\n", s);
		
		System.out.println("\nAufgabe 2\n");
		String x="*";
		System.out.printf("%7s\n", x);
		System.out.printf("%6s%s%s\n", x, x, x);
		System.out.printf("%5s%s%s%s%s\n", x, x, x, x, x);
		System.out.printf("%4s%s%s%s%s%s%s\n", x, x, x, x, x, x, x);
		System.out.printf("%3s%s%s%s%s%s%s%s%s\n", x, x, x, x, x, x, x, x, x);
		System.out.printf("%2s%s%s%s%s%s%s%s%s%s%s\n", x, x, x, x, x, x, x, x, x, x, x);
		System.out.printf("%s%s%s%s%s%s%s%s%s%s%s%s%s\n", x, x, x, x, x, x, x, x, x, x, x, x, x);
		System.out.printf("%6s%s%s\n", x, x, x);
		System.out.printf("%6s%s%s\n", x, x, x);
		
		System.out.println("\nAufgabe 3\n");
		double a=22.4234234;
		double b=111.2222;
		double c=4.0;
		double d=1000000.551;
		double e=97.34;
		System.out.printf("%.2f\n", a);
		System.out.printf("%.2f\n", b);
		System.out.printf("%.2f\n", c);
		System.out.printf("%.2f\n", d);
		System.out.printf("%.2f\n", e);
		
		
	}

}


public class Captain {

	// Attribute
	private String name;
	private String surname;
	private int captainYears;
	private double gehalt;
	// TODO: 3. Fuegen Sie in der Klasse 'Captain' das Attribut 'name' hinzu und
	// implementieren Sie die entsprechenden get- und set-Methoden.
	

	// Konstruktoren

	// TODO: 4. Implementieren Sie einen Konstruktor, der die Attribute surname
	// und name initialisiert.
	
	public Captain(String name, String surname) {
		this.name = name;
		this.surname = surname;
	}
	// TODO: 5. Implementieren Sie einen Konstruktor, der alle Attribute
	// initialisiert.
	
	public Captain(String name, String surname, int captainYears, double gehalt) {
		this(name, surname);
		setCaptainYears(captainYears);
		setSalary(gehalt);
	}
	
	public Captain() {
		this.surname = null;
		this.name = null;
		this.captainYears = 0;
		this.gehalt = 0.0;
	}

	// Verwaltungsmethoden
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getSurname() {
		return surname;
	}

	public void setSalary(double gehalt) {
		// TODO: 1. Implementieren Sie die entsprechende set-Methode.
		// Ber�cksichtigen Sie, dass das Gehalt nicht negativ sein darf.
		if(gehalt >= 0)
			this.gehalt = gehalt;
		else
			System.out.println("Das Gehalt darf nicht 0 oder kleiner sein!");
			this.gehalt = 10;
	}

	public double getSalary() {
		// TODO: 2. Implementieren Sie die entsprechende get-Methode.
		return this.gehalt;
	}

	// TODO: 6. Implementieren Sie die set-Methode und die get-Methode f�r
	// captainYears.
	// Ber�cksichtigen Sie, dass das Jahr nach Christus sein soll.
	public void setCaptainYears(int captainYears) {
		if(captainYears>0)
			this.captainYears = captainYears;
		else
			System.out.println("Das Jahr soll nach Christus sein!");
	}
	
	public int getCaptainYears() {
		return captainYears;
	}
	// Weitere Methoden
	
	// TODO: 7. Implementieren Sie eine Methode 'vollname', die den vollen Namen
	// (Vor- und Nachname) als string zur�ckgibt.
	public String vollname() {
		return this.name + "" + this.surname;
	}

	public String toString() {  // overriding the toString() method
		// TODO: 8. Implementieren Sie die toString() Methode.
		return name + " " + surname + " " + captainYears + " " + gehalt;
	}

}